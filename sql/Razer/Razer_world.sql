-- Fix Full House - Reanimated Adherent - Reanimated Fanatic
DELETE FROM `spell_script_names` WHERE spell_id in (72495,72496,72497);
INSERT INTO `spell_script_names` VALUES
(72495, 'spell_cultist_dark_martyrdom'),
(72496, 'spell_cultist_dark_martyrdom'),
(72497, 'spell_cultist_dark_martyrdom');

-- Rotface:
-- Remove conditions (target selection changed from TARGET_UNIT_NEARBY_ENTRY to TARGET_UNIT_TARGET_ANY so doesn't need it anymore)
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry`=69508;

/* backup data
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 0, 69508, 0, 0, 18, 0, 1, 37986, 0, 0, 0, '', 'Rotface - Slime Spray');
*/

-- Add script
DELETE FROM `spell_script_names` WHERE `ScriptName`='spell_rotface_slime_spray';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(69507, 'spell_rotface_slime_spray'),
(71213, 'spell_rotface_slime_spray'),
(73189, 'spell_rotface_slime_spray'),
(73190, 'spell_rotface_slime_spray');

-- Fix targetting for Ooze Flood ability in encounter Modermiene / Rotface in instance / Instanz ICC / Eiskronenzitadelle / Icecrown Citadel
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceGroup` = 3 AND `SourceEntry` IN (69783, 69797, 69799, 69802) AND `ConditionTypeOrReference` = 33;
INSERT INTO `conditions` (SourceTypeOrReferenceId, SourceGroup, SourceEntry, SourceId, ElseGroup, ConditionTypeOrReference, ConditionTarget, ConditionValue1, ConditionValue2, ConditionValue3, NegativeCondition, ErrorTextId, ScriptName, Comment) VALUES
(13, 3, 69783, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self'),
(13, 3, 69797, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self'),
(13, 3, 69799, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self'),
(13, 3, 69802, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self');

-- DB/NPCs: Little Ooze (Rotface in ICC) can not be taunted, 2012_09_10_06_world_creature_template.sql
UPDATE `creature_template` SET `flags_extra`=`flags_extra`|256 WHERE `entry` IN (36897, 38138); -- Little Ooze

-- Volatile Ooze - Gas Cloud Movement Speed
UPDATE `creature_template` SET speed_walk = "0.6" WHERE `entry` IN (37697,38604,38758,38759);
UPDATE `creature_template` SET speed_walk = "0.8" WHERE `entry` IN (37562,38602,38760,38761);
UPDATE `creature_template` SET speed_run = "0.6" WHERE `entry` IN (37697,38604,38758,38759);
UPDATE `creature_template` SET speed_run = "0.8" WHERE `entry` IN (37562,38602,38760,38761);

-- Fix Precious and Stinky Respawn Time
UPDATE `creature` SET `spawntimesecs` =  '604800' WHERE `creature`.`id` =37025;
UPDATE `creature` SET `spawntimesecs` =  '604800' WHERE `creature`.`id` =37217;

-- Fix Val'kyr Shadowguard Speed 
UPDATE `creature_template` SET speed_walk = "0.242857" WHERE entry IN (36609,39120,39121,39122);
UPDATE `creature_template` SET speed_run = "0.242857" WHERE entry IN (36609,39120,39121,39122);

-- Sindragosa: Fix spell 69762 Unchained Magic - Add internal cooldown with 1 seconds, so multi-spell spells will only apply one stack of triggered spell 69766 (i.e. Chain Heal)
DELETE FROM `spell_proc_event` WHERE `entry` = 69762;
INSERT INTO `spell_proc_event` (entry, SchoolMask, SpellFamilyName, SpellFamilyMask0, SpellFamilyMask1, SpellFamilyMask2, procFlags, procEx, ppmRate, CustomChance, Cooldown) VALUES
(69762, 0, 0, 0, 0, 0, 81920, 0, 0, 0, 1);

UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (37955,38434,38435,38436); -- Blood-Queen Lana'thel (ICC)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (38004); -- Blood-Queen Lana'thel (Event)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (36853,38265,38266,38267); -- Sindragosa (ICC)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (36609, 39120, 39121, 39122); -- Valkyr Shadowguard
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (37126,38258); -- Sister Svalna
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (37533,38220); -- Rimefang (ICC)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (37534,38219); -- Spinestalker (ICC)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (39747,39823); -- Saviana Ragefire (RS)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (10184,36538); -- Onyxia (Ony)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (24068,31655); -- Annhylde the Caller (UK)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (11583); -- Nefarian
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (27829); -- Ebon Gargyole

-- Update ICC: Rotface - Fix targetting for Ooze Flood ability in encounter Modermiene / Rotface in instance / Instanz ICC / Eiskronenzitadelle / Icecrown Citadel
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceGroup` = 3 AND `SourceEntry` IN (69783, 69797, 69799, 69802) AND `ConditionTypeOrReference` = 33;
INSERT INTO `conditions` (SourceTypeOrReferenceId, SourceGroup, SourceEntry, SourceId, ElseGroup, ConditionTypeOrReference, ConditionTarget, ConditionValue1, ConditionValue2, ConditionValue3, NegativeCondition, ErrorTextId, ScriptName, Comment) VALUES
(13, 3, 69783, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self'),
(13, 3, 69797, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self'),
(13, 3, 69799, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self'),
(13, 3, 69802, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self');

-- Add immunities to Little Ooze / Big Ooze
UPDATE `creature_template` SET `mechanic_immune_mask` = 650853247 WHERE `entry` IN (36897, 36899, 38138, 38123);

-- Add interrupt immunity to mini bosses (Rimefang / Raufang)
UPDATE `creature_template` SET `mechanic_immune_mask` = 650853247 WHERE `entry` IN (37533, 38220, 37534, 38219);

-- DB/NPCs: Little Ooze (Rotface in ICC) can not be taunted.
UPDATE `creature_template` SET `flags_extra`=`flags_extra`|256 WHERE `entry` IN (36897, 38138);

-- Set speed values to database for Vile Spirits and InhabitType
UPDATE `creature_template` SET `speed_walk` = 0.5, `speed_run` = 0.5, `InhabitType` = 4 WHERE `entry` IN (37799, 39284, 39285, 39286);

-- Halion immunities
UPDATE creature_template SET mechanic_immune_mask = mechanic_immune_mask | 650854271 WHERE entry IN (
39863, 40142, -- 10 nm
39864, 40143, -- 25 nm
39944, 40144, -- 10 hc
39945, 40145  -- 25 hc
);

-- Halion damage multipliers
UPDATE creature_template SET dmg_multiplier = 70 WHERE entry IN (39863, 40142);  -- 10 nm
UPDATE creature_template SET dmg_multiplier = 100 WHERE entry IN (39864, 40143); -- 25 nm
UPDATE creature_template SET dmg_multiplier = 100 WHERE entry IN (39944, 40144); -- 10 hc
UPDATE creature_template SET dmg_multiplier = 170 WHERE entry IN (39945, 40145); -- 25 hc

-- General Zarithrian damage multipliers
UPDATE `creature_template` SET `mindmg`=497, `maxdmg`=676, `attackpower`=795, `dmg_multiplier`=50 WHERE `entry`=39746;
UPDATE `creature_template` SET `mindmg`=497, `maxdmg`=676, `attackpower`=795, `dmg_multiplier`=90 WHERE `entry`=39805;

-- Saviana Ragefire damage multipliers
UPDATE `creature_template` SET `mindmg`=497, `maxdmg`=676, `attackpower`=795, `dmg_multiplier`=50 WHERE `entry`=39747;
UPDATE `creature_template` SET `mindmg`=497, `maxdmg`=676, `attackpower`=795, `dmg_multiplier`=90 WHERE `entry`=39823;

-- Fix Halion - Orb Rotation Focus visible for players
UPDATE `creature_template` SET `modelid1`=169, `modelid2`=11686, `flags_extra` = `flags_extra` | 128 WHERE `entry` IN (40091, 43280, 43281, 43282);

-- Dancing Rune No Attack No Select
UPDATE `creature_template` SET unit_flags = "33685504" WHERE entry = "27893";

-- Removes Master's Call stun immunity.
INSERT INTO `spell_linked_spell`(`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (54216,-56651,1,'Removes Master''s Call stun immunity');

-- Disarm Bladestorm
DELETE FROM `spell_linked_spell` WHERE  `spell_trigger`=51722 AND `spell_effect`=-46924 ;
DELETE FROM `spell_linked_spell` WHERE  `spell_trigger`=676 AND `spell_effect`=-46924;
INSERT INTO `spell_linked_spell` VALUES (676, -46924, 1, '(War)Disarm Cancel Bladestorm');
INSERT INTO `spell_linked_spell` VALUES (51722, -46924, 1, '(Rogue)Dismantle Cancel Bladestorm');
INSERT INTO `spell_linked_spell` VALUES (64058, -46924, 1, '(Priest) Psychic Horror Cancel Bladestorm');
DELETE FROM `spell_linked_spell` WHERE  `spell_trigger`=64346 AND `spell_effect`=-46924;
INSERT INTO `spell_linked_spell` VALUES (64346, -46924, 1, '(Mage)Fiery Payback Cancel Bladestorm');
DELETE FROM `spell_linked_spell` WHERE  `spell_trigger`=53359 AND `spell_effect`=-46924;
INSERT INTO `spell_linked_spell` VALUES (53359, -46924, 1, '(Hunter)Chimera Shot(scorpid) Cancel Bladestorm');

-- Fix Shadowmeld.
DELETE FROM spell_script_names WHERE spell_id = 58984;
INSERT INTO spell_script_names VALUES (58984, 'spell_gen_shadowmeld');

-- Fix Rogue T10 4p bonus
DELETE FROM spell_proc_event WHERE entry = 70803;
INSERT INTO `spell_proc_event` VALUES('70803', '0', '8', '4063232', '8', '0', '0', '0', '0', '0', '0');

-- Add MC Immunity to all NPC's
UPDATE `creature_template` SET mechanic_immune_mask = mechanic_immune_mask + 1 WHERE entry >0;

-- Bags and Totem at Character Creation
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('0','1','5765','4');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('0','2','5765','4');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('0','4','5765','4');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('0','3','5765','3');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('0','5','5765','4');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('0','7','5765','4');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('0','8','5765','4');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('0','11','5765','4');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('0','9','5765','4');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('11','7','46978','1');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('2','7','46978','1');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('6','7','46978','1');
insert into `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) values('8','7','46978','1');

-- Stop Bladestorm when Hand of Protection buff is given
DELETE FROM `spell_linked_spell` WHERE `spell_effect`=-46924;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES
(1022, -46924, 1, 'Hand of protection (Rank1) - remove bladestorm'),
(5599, -46924, 1, 'Hand of protection (Rank2) - remove bladestorm'),
(10278, -46924, 1, 'Hand of protection (Rank3) - remove bladestorm'),
(66009, -46924, 1, 'Hand of protection (rank1 dupe) - remove bladestorm');

-- Fix Cyclone on Cyclone and Faerie Fire on Cyclone
insert into `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) values('33786','-33786','2','Cyclone - Cyclone');
insert into `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) values('33786','-16857','2','Cyclone - Faerie Fire (Feral)');
insert into `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) values('33786','-770','2','Clyclone - Faerie fire');

-- Fix Brain Freeze - Fingers of Frost
UPDATE `spell_proc_event` SET `procEx` = 0x0040000 WHERE `entry` IN (44546,44548, 44549, 44543, 44545);

-- Fix a bug when pets chasing target even if it's invisible
DELETE FROM `spell_linked_spell` WHERE `spell_effect` = 54661 AND `spell_trigger` IN (32612,5215,1784);
INSERT INTO `spell_linked_spell` VALUES
(32612,54661,0,'Invisibility Sanctuary Effect'),
(5215,54661,0,'Prowl Sanctuary Effect'),
(1784,54661,0,'Stealth Sanctuary Effect');

-- Fix Killing Machine PPM
UPDATE `spell_proc_event` SET cooldown = "5" WHERE entry IN (51123, 51127, 51128, 51129, 51130);

-- Fix ToC Raid and ToC Heroic Maps
delete from `disables` where entry in (649,650);
insert into `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) values('7','649','0','','','Disable Mmap - Trial of the Crusader');
insert into `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) values('7','650','7','','','Disable Mmap - Trial of the Champion');

-- Custom Stackeables Values
UPDATE `item_template` SET Stackable = "1000" WHERE entry = "21177";
UPDATE `item_template` SET Stackable = "100" WHERE entry = "9186";
UPDATE `item_template` SET Stackable = "100" WHERE entry = "44605";
UPDATE `item_template` SET Stackable = "100" WHERE entry = "44614";
UPDATE `item_template` SET Stackable = "100" WHERE entry = "3776";
UPDATE `item_template` SET Stackable = "100" WHERE entry = "43231";
UPDATE `item_template` SET Stackable = "100" WHERE entry = "43233";
UPDATE `item_template` SET Stackable = "100" WHERE entry = "43235";
UPDATE `item_template` SET Stackable = "100" WHERE entry = "43237";
UPDATE `item_template` SET Stackable = "100" WHERE entry = "44615";
UPDATE `item_template` SET Stackable = "20" WHERE entry = "17033";
UPDATE `item_template` SET Stackable = "5" WHERE entry = "6265";

-- Crossfaction Bg Minplayersperteam
UPDATE `battleground_template` SET Minplayersperteam = "2", Weight = "7" WHERE id IN (2,3,7,9,32);

-- Disable Alterac Valley - Isle of Conquest
insert into `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) values('3','1','0','','','Battleground - Alterac Valley');
insert into `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) values('3','30','0','','','Battleground - Isle of Conquest');

-- Enable The Ring of Valor Arena
DELETE FROM `disables` WHERE entry = "11";

-- Update ALL ICC Immunities
UPDATE `creature_template` SET `mechanic_immune_mask`=`mechanic_immune_mask`
|1 -- CHARM
|2 -- DISORIENTED
|4 -- DISARM
|8 -- DISTRACT
|16 -- FEAR
|32 -- GRIP
|64 -- ROOT
|256 -- SILENCE
|512 -- SLEEP
|1024 -- SNARE
|2048 -- STUN
|4096 -- FREEZE
|8192 -- KNOCKOUT
|65536 -- POLYMORPH
|131072 -- BANISH
|524288 -- SHACKLE
|4194304 -- TURN
|8388608 -- HORROR
|67108864 -- DAZE
|536870912 -- SAPPED
WHERE `entry` IN
(36612,37957,37958,37959, -- Lord Marrowgar
36619,38233,38459,38460, -- Bone Spike
36855,38106,38296,38297, -- Lady Deathwhisper
38490,38494, -- Root Giante
37813,38402,38582,38583, -- Deathbringer Saurfang
36626,37504,37505,37506, -- Festergut
36627,38390,38549,38550, -- Rotface
36897,38138, -- Little Ooze
36899,38123, -- Big Ooze
36678,38431,38585,38586, -- Professor Putricide
37697,38604,38758,38759, -- Volatile Ooze
37562,38602,38760,38761, -- Gas Cloud
37970,38401,38784,38785, -- Prince Valanar
37972,38399,38769,38770, -- Prince Keleseth
37973,38400,38771,38772, -- Prince Taldaram
38369, -- Dark Nucleus 
38454,38775,38776,38777, -- Kinetic Bomb
37955,38434,38435,38436, -- Blood-Queen Lana'thel
37126,38258, -- Sister Svalna
37533,38220, -- Rimefang
37534,38219, -- Spinestalker
36853,38265,38266,38267, -- Sindragosa
36597,39166,39167,39168, -- The Lich King
37698,39299,39300,39301, -- Shambling Horror
37695,39309,39310,39311, -- Drudge Ghoul
36633,39305,39306,39307, -- Ice Sphere
36701,39302,39303,39304, -- Raging Spirit
36609,39120,39121,39122, -- Val'kyr Shadowguard
37799, 39284, 39285, 39286, -- Vile Spirits
39190,39287,39288,39289, -- Wicked Spirit
36824,39296); -- Spirit Warden

-- Fix ICC Maps
delete from `disables` where entry in (631);
insert into `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) values('7','631','0','','','Disable Mmap - Icecrown Citadel');
